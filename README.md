# Integradora2

## Project: Estetica Selene

Figma design page: https://www.figma.com/file/oBy1VwFaIJEGePQKBe8A6e/Wireframing-in-Figma?node-id=0%3A1 

- Problem it solves 
    >Solve the problem of having to go to the store or call to schedule an appointment by doing everything from a web page and instantly
- History of the problem
    >
- Target market served by the solution
    >anyone looking to have a scheduled haircut for various reasons
- Identification of the problem (wording in question form)
    >
- Solution Statement
    >A Web application will be developed
- General objective
    >Make so that the costumer has quick access to schedule appointments
    >- Principal page ()
    >- Services tab (Easy access to a list of services offered)
    >- Login and register (Register to save your data and get more contact and help if required)
- Project justification
    >

## TEAM MEMBERS
- Team member #1 @Andres_Raso - a20311079@uthermosillo.edu.mx
- Team member #2 @pzkevin - a20311111@uthermosillo.edu.mx
